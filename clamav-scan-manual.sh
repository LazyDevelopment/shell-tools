#!/bin/bash

## This is a simplification placeholder to run the ClamAV command with right parameters to scan some folder or file manually
## Requires:
##  - vt tool installed in the $PATH (can be found on `https://github.com/VirusTotal/vt-cli`)
##  - `shasum` tool should be installed (double-check it on Windows/Cygwin)
##  - The script has to be executed with SUDO
## Usage: `clamav-scan-manual /path/to/item [-s]`
##        -s   stop ClamD after finish scan job

## Use next command to debug the stuck scan job: `for item in *; do clamav-scan-manual "./$item"; done`
## Run it from some parent folder of stuck lob's last scanned file. Repeat in different folders to find the cause

# Check if a parameter was provided
if [ $# -lt 1 ] || [ $# -gt 2 ]; then
    echo "Usage: ${0} /path/to/item [-s]"
    echo "       -s   stop ClamD after finish scan job"
    exit 1
fi

# Check if the script is running as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root or with 'sudo':"
  echo "$ sudo ${0} ${@}"
  exit
fi

stop_svc="${2}"
scan_path=$(realpath "${1}")

# OS-specific variables
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux
  quarantine="/var/opt/infected/"
  config="/usr/local/etc/clamd.conf"
  log="/var/log/clamav/clamdscan_$( date +%Y-%m-%d_%H-%M-%S ).log"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # MacOS
  quarantine="/usr/local/infected/"
  config="/usr/local/etc/clamav/clamd.conf"
  log="/usr/local/var/log/clamav/clamdscan_$( date +%Y-%m-%d_%H-%M-%S ).log"
elif [[ "$OSTYPE" == "cygwin" ]]; then
  echo "Windows/Cygwin platform is not tested yet, so not supported."
  exit 1
elif [[ "$OSTYPE" == "msys" ]]; then
  echo "Windows/MSYS platform is not tested yet, so not supported."
  exit 1
elif [[ "$OSTYPE" == "win32" ]]; then
  echo "Windows platform is not tested yet, so not supported."
  exit 1
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  echo "FreeBSD platform is not tested yet, so not supported."
  exit 1
else
  echo "Unknown OS: $OSTYPE. Platform cannot be supported."
  exit 1
fi

start_clamd_linux() {
  if [ -z "$( pgrep '^clamd$' )" ]; then
    echo "Starting the ClamD service"
    systemctl start clamd.service
    echo "Waiting for service to start"
    sleep 60
  fi
}
stop_clamd_linux() {
  systemctl stop clamd.service
}

start_clamd_mac() {
  if [ -z "$( pgrep '^clamd$' )" ]; then
    echo "Starting the ClamD service"
    launchctl load /Library/LaunchDaemons/net.clamav.clamd.plist
    launchctl start net.clamav.clamd
    echo "Waiting for service to start"
    sleep 60
  fi
}
stop_clamd_mac() {
  launchctl stop net.clamav.clamd
  kill "$( pgrep '^clamd$' )"
}

cleanup() {
  if [[ "${stop_svc}" == "-s" ]]; then
    echo "Shutting down the ClamD service"
    if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      stop_clamd_linux
    elif [[ "$OSTYPE" == "darwin"* ]]; then
      stop_clamd_mac
    fi
  fi
}
# Use trap to call cleanup() when the script exits
trap cleanup EXIT

# Function to check if a file is a virus report
is_virus_report() {
  [[ "${1}" =~ \.vt-result-page\.txt$ || "${1}" =~ \.vt-result\.yml$ ]]
}
# Function to check if a file is already scanned by VirusTotal
is_already_scanned() {
  [[ -f "${1}".vt-result-page.txt || -f "${1}".vt-result.yml ]]
}

# Check if the path is valid
if [ -e "${scan_path}" ]; then
  # Check if provided path is not an empty folder / file
  if [ -d "${scan_path}" ] && [ -z "$(ls -A "${scan_path}")" ]; then
    echo "The '${scan_path}' is an empty folder. Nothing to scan."
    exit 0
  elif [[ -f "${scan_path}" && ! -s "${scan_path}" ]]; then
    echo "The '${scan_path}' is an empty file. Nothing to scan."
    exit 0
  fi

  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    start_clamd_linux
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    start_clamd_mac
  fi

  echo "Starting the '${scan_path}' scanning job"
  clamdscan \
    --config-file "${config}" \
    --wait --ping 5 \
    --fdpass \
    --log "${log}" --verbose \
    --copy "${quarantine}" \
    --allmatch \
    --multiscan \
    "${scan_path}"
  chmod +r "${log}"
  echo "CalmAV sacn log is available in the '${log}'"

  # Iterate over all files in the quarantine directory
  if [ "$(ls -A "${quarantine}")" ]; then
    echo "Scanning files in the '${quarantine}' with VirusTotal"
    for file in "${quarantine}"/*; do
      # Skip non-regular files, virus reports, and already scanned files
      if ! [[ -f "${file}" ]] || is_virus_report "${file}" || is_already_scanned "${file}"; then
        continue
      fi

      # Calculate the SHA256 hash of the file
      sha256=$( shasum -a 256 "${file}" | awk '{ print $1 }' )

      # Check if the 'shasum' command succeeded
      if [ -n "$sha256" ]; then
        echo "Check if the '${file}' is in VirusTotal DB"
        vt_result=$( vt file "$sha256" 2>&1 )

        # If the file is not in VirusTotal, scan it
        if [[ $vt_result == *"not found"* ]]; then
          echo "The file not found in the DB, sending it to VirusTotal for scanning"
          vt scan file --wait --open "${file}" | awk '{ print $2 }' > "${file}".vt-result-page.txt
          chmod +r "${file}".vt-result-page.txt
          echo "Scan result can be found on the VirusTotal site. Direct link is in the '${file}.vt-result-page.txt':"
          cat "${file}".vt-result-page.txt
        else
          echo "${vt_result}" > "${file}".vt-result.yml
          chmod +r "${file}".vt-result.yml
          echo "The file is found in the DB. The latest scan result is in the '${file}.vt-result.yml'"
        fi
      else
        echo "Failed to calculate the SHA256 hash of $file"
      fi
    done
  else
    echo "Quarantine directory is empty"
  fi
else
  echo "The path '${scan_path}' is not valid or not accessible"
  exit 1
fi
