#!/bin/bash

## This is a simplification placeholder to scan some folder or file by VirusTotal manually
## Requires:
##  - vt tool installed in the $PATH (can be found on `https://github.com/VirusTotal/vt-cli`)
##  - `shasum` tool should be installed (double-check it on Windows/Cygwin)
##  - The script has to be executed under the user, who is able to read the files or with SUDO
## Usage: `vt-check-quarantine /path/to/item`

# Check if a parameter was provided
if [ "$#" -ne 1 ]; then
    echo "Usage: ${0} /path/to/item"
    exit 1
fi

quarantine=$(realpath "${1}")

scan_file() {
  file="$1"
  # Calculate the SHA256 hash of the file
  sha256=$(shasum -a 256 "${file}" | awk '{ print $1 }')

  # Check if the 'shasum' command succeeded
  if [ -n "$sha256" ]; then
    echo "Check if the '${file}' is in VirusTotal DB"
    vt_result=$(vt file "$sha256" 2>&1)

    # If the file is not in VirusTotal, scan it
    if [[ $vt_result == *"not found"* ]]; then
      echo "The file not found in the DB, sending it to VirusTotal for scanning"
      vt scan file --wait --open "${file}" | awk '{ print $2 }' > "${file}".vt-result-page.txt
      echo "Scan result can be found on the VirusTotal site. Direct link is in the '${file}.vt-result-page.txt'"
    else
      echo "${vt_result}" > "${file}".vt-result.yml
      echo "The file is found in the DB. The latest scan result is in the '${file}.vt-result.yml'"
    fi
  else
    echo "Failed to calculate the SHA256 hash of $file"
  fi
}

# Check if the path is valid
if [ -d "${quarantine}" ] && [ "$(ls -A "${quarantine}")" ]; then
    # Iterate over all files in the quarantine directory
    for file in "${quarantine}"/*
    do
      scan_file "${file}"
    done
elif [ -f "${quarantine}" ]; then
  # If the path to file, scan it
  scan_file "${quarantine}"
else
  echo "The path '${quarantine}' is empty, not valid, or not accessible"
  exit 1
fi
