#!/bin/bash

# Do some File System level cleanup on Mac OS X

echo "Script is executed with next user permissions"
id -p

date
echo "Telegram"
telegramContainer=$(find "${HOME}/Library/Group Containers" -type d -maxdepth 1 -name "*.keepcoder.Telegram")
telegramAccounts=$(find "${telegramContainer}/stable" -type d -maxdepth 1 -name "account-*")
IFS=$'\n'
# shellcheck disable=SC2048
for telegramAccount in ${telegramAccounts}; do
  echo "-- Account: ${telegramAccount}"
  count=$(find "${telegramAccount}/postbox/media" -type f -mtime +30 -print | wc -l)
  size=$(find "${telegramAccount}/postbox/media" -type f -mtime +30 -exec du -ch {} + | grep total$ || echo "0B")
  find "${telegramAccount}/postbox/media" -type f -mtime +30 -exec rm -f {} \;
  echo "${count} files removed. ${size} space is free."
done
echo
echo "WhatsApp cache"
count=$(find "${HOME}/Library/Application Support/WhatsApp/Cache" -type f -mtime +30 -print | wc -l)
size=$(find "${HOME}/Library/Application Support/WhatsApp/Cache" -type f -mtime +30 -exec du -ch {} + | grep total$ || echo "0B")
find "${HOME}/Library/Application Support/WhatsApp/Cache" -type f -mtime +30 -exec rm -f {} \;
echo "${count} files removed. ${size} space is free."
echo
echo "WhatsApp CacheStorage"
count=$(find "${HOME}/Library/Application Support/WhatsApp/Service Worker/CacheStorage" -type f -name "*_0" -mtime +30 -print | wc -l)
size=$(find "${HOME}/Library/Application Support/WhatsApp/Service Worker/CacheStorage" -type f -name "*_0" -mtime +30 -exec du -ch {} + | grep total$ || echo 0B)
find "${HOME}/Library/Application Support/WhatsApp/Service Worker/CacheStorage" -type f -name "*_0" -mtime +30 -exec rm -f {} \;
echo "${count} files removed. ${size} space is free."
echo
echo "Desktop pictures"
picFolder=$(find "/private/var/folders" -type d -maxdepth 4 -name "com.apple.desktoppicture" 2>/dev/null)
count=$(find "${picFolder}" -type f -mtime +30 -print  | wc -l)
size=$(find "${picFolder}" -type f -mtime +30 -exec du -ch {} + | grep total$ || echo "0B")
find "${picFolder}" -type f -mtime +30 -exec rm -f {} \;
echo "${count} files removed. ${size} space is free."
echo
date
echo "--- completed ---"
