#!/bin/bash

# Prepare environment
SCRIPTDIR="$(dirname "$0")"
apt-get update && apt-get install -y jq fzf
chmod +x "${SCRIPTDIR}/../npm-upgrade.sh"

# Install packages for testing
npm install --global npm corepack@0.10.0 async@3.2.0 lodash@4.17.10 cloudinary@1.30.0

# Upgrade all outdated packages
if "${SCRIPTDIR}/../npm-upgrade.sh" -d -f -a; then
  echo npm-upgrade has been executed successfully
else
  echo npm-upgrade has failed with some error
  exit 1
fi

# Verify result
outdated="$(npm outdated --global)"
npmlist="$(npm list -g --depth 0)"
if [ -n "${outdated}" ] \
    && [[ "$(echo "${npmlist}" | grep corepack | tr -d '`├─└+-' | xargs)" == "corepack@0.10.0" ]] \
    && [[ "$(echo "${npmlist}" | grep async | tr -d '`├─└+-' | xargs)" == "async@3.2.0" ]] \
    && [[ "$(echo "${npmlist}" | grep lodash | tr -d '`├─└+-' | xargs)" == "lodash@4.17.10" ]] \
    && [[ "$(echo "${npmlist}" | grep cloudinary | tr -d '`├─└+-' | xargs)" == "cloudinary@1.30.0" ]]; then
  echo PASS No packages updated, Dry-run executed
else
  echo FAIL Some packages updated
  echo "${outdated}"
  echo npm list
  echo "${npmlist}"
  exit 1
fi
