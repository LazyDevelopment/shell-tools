#!/bin/bash

# Prepare environment
SCRIPTDIR="$(dirname "$0")"
apt-get update && apt-get install -y fzf curl git
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
export NVM_LAZY=1
export NVM_DIR="${HOME}/.nvm"
# shellcheck disable=SC1091
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
node_version="$(node --version)"
if [ -n "${node_version}" ]; then
  nvm deactivate
  nvm uninstall "${node_version}"
fi
chmod +x "${SCRIPTDIR}/../node-upgrade.sh"

# Install packages for testing
nvm install v18.18.2
nvm alias default stable
#nvm install-latest-npm
npm install --global async lodash redis

# Upgrade node
if "${SCRIPTDIR}/../node-upgrade.sh" --keep-default --latest; then
  echo node-upgrade has been executed successfully
else
  echo node-upgrade has failed with some error
  exit 1
fi

# Verify result
# shellcheck disable=SC1091
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
node_version="$(node --version)"
npmlist="$(npm list -g --depth 0)"
if [ -z "${node_version}" ] \
    || [[ "${node_version}" == "v18.18.2" ]] \
    || [[ "$(nvm ls | grep default | head -n 1)" == *"node"* ]] \
    || [[ "$(npm --version)" == "9.8.1" ]] \
    || [[ "$(echo "${npmlist}" | grep async | tr -d '`├─└+-' | xargs)" != *"async"* ]] \
    || [[ "$(echo "${npmlist}" | grep lodash | tr -d '`├─└+-' | xargs)" != *"lodash"* ]] \
    || [[ "$(echo "${npmlist}" | grep redis | tr -d '`├─└+-' | xargs)" != *"redis"* ]]; then
  echo FAIL Node was not updated, NPM packages not re-installed or NVM default alias was changed
  echo "${node_version}"
  echo NPM version
  npm --version
  echo npm list
  echo "${npmlist}"
  echo NVM ls
  nvm ls
  exit 1
else
  echo PASS Node updated, all packages re-installed and NVM default alias was not changed
fi
