#!/bin/bash

# Prepare environment
SCRIPTDIR="$(dirname "$0")"
apt-get update && apt-get install -y jq fzf
chmod +x "${SCRIPTDIR}/../pip-upgrade.sh"

# Install packages for testing
pip install --no-cache-dir -U pip setuptools wheel flake8==5.0.0 XlsxWriter==2.0.0 requests==2.28.0 redis==4.2.0

# Upgrade all outdated packages
if "${SCRIPTDIR}/../pip-upgrade.sh" -f -p requests -p redis; then
  echo pip-upgrade has been executed successfully
else
  echo pip-upgrade has failed with some error
  exit 1
fi

# Verify result
outdated="$(pip list --outdated --not-required)"
piplist="$(pip list)"
if [[ "${outdated}" == *"requests"* ]] || [[ "${outdated}" == *"redis"* ]] \
    || [[ "$(echo "${piplist}" | grep flake8 | xargs)" != "flake8 5.0.0" ]] \
    || [[ "$(echo "${piplist}" | grep XlsxWriter | xargs)" != "XlsxWriter 2.0.0" ]] \
    || [[ "$(echo "${piplist}" | grep requests | xargs)" == "requests 2.28.0" ]] \
    || [[ "$(echo "${piplist}" | grep redis | xargs)" == "redis 4.2.0" ]]; then
  echo FAIL Not expected versions of packages detected
  echo "${outdated}"
  echo pip list
  echo "${piplist}"
  exit 1
else
  echo PASS All packages have expected versions
fi
