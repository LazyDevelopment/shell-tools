#!/bin/bash

# Prepare environment
SCRIPTDIR="$(dirname "$0")"
apt-get update && apt-get install -y fzf curl git
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
export NVM_LAZY=1
export NVM_DIR="${HOME}/.nvm"
# shellcheck disable=SC1091
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
node_version="$(node --version)"
if [ -n "${node_version}" ]; then
  nvm deactivate
  nvm uninstall "${node_version}"
fi
chmod +x "${SCRIPTDIR}/../node-upgrade.sh"

# Install packages for testing
nvm install v18.18.2
#nvm install-latest-npm
npm install --global async lodash redis
# shellcheck disable=SC2011
nvmls="$(ls "${NVM_DIR}/versions/node/" | xargs)"
IFS=$' '
orig_vers_count=0
read -ra strarr <<< "${nvmls}"
for val in "${strarr[@]}";
do
  if [[ ${val} =~ ^v([0-9]+).([0-9]+).([0-9]+) ]]; then
    orig_vers_count=$(( orig_vers_count + 1 ))
  fi
done

# Upgrade node
if "${SCRIPTDIR}/../node-upgrade.sh" --keep-old --exact v20.9.0; then
  echo node-upgrade has been executed successfully
else
  echo node-upgrade has failed with some error
  exit 1
fi

# Verify result
# shellcheck disable=SC1091
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm use v20.9.0 || echo "FAIL Unable to use new Node version"
node_version="$(node --version)"
npmlist="$(npm list -g --depth 0)"
nvmls="$(nvm ls)"
IFS=$' '
new_vers_count=0
# shellcheck disable=SC2011
read -ra strarr <<< "$(ls "${NVM_DIR}/versions/node/" | xargs)"
for val in "${strarr[@]}";
do
  if [[ ${val} =~ ^v([0-9]+).([0-9]+).([0-9]+) ]]; then
    new_vers_count=$(( new_vers_count + 1 ))
  fi
done
if [ -z "${node_version}" ] \
    || [[ "${node_version}" != "v20.9.0" ]] \
    || [[ "$(echo "${nvmls}" | grep default | head -n 1)" != *"node"* ]] \
    || [[ "$(npm --version)" == "9.8.1" ]] \
    || [[ "${new_vers_count}" != "$((orig_vers_count + 1))" ]] \
    || [[ "$(echo "${npmlist}" | grep async | tr -d '`├─└+-' | xargs)" != *"async"* ]] \
    || [[ "$(echo "${npmlist}" | grep lodash | tr -d '`├─└+-' | xargs)" != *"lodash"* ]] \
    || [[ "$(echo "${npmlist}" | grep redis | tr -d '`├─└+-' | xargs)" != *"redis"* ]]; then
  echo "FAIL Node was not updated or NPM packages not re-installed, counts of Node versions do not match"
  echo "${node_version}"
  echo NPM version
  npm --version
  echo npm list
  echo "${npmlist}"
  echo NVM ls
  echo "${nvmls}"
  echo "Your NVM setup originally had ${orig_vers_count} Node versions, but now has ${new_vers_count}, while expected $((orig_vers_count + 1))"
  exit 1
else
  echo PASS Node updated and all packages re-installed, old Node was not removed
fi
