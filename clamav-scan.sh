#!/bin/bash

## ClamAV integration script for Wazuh to run the AV check for new or modified file

# File path from stdin
read -r alert
# Extract the file path from the alert data
FILE_PATH=$( echo "$alert" | grep -oP "File '\K[^']*" )

# Create a new directory for each hour
CLAMSCAN_LOG="/var/log/clamav"
LOG_DIR="${CLAMSCAN_LOG}/$(date +%Y-%m-%d_%H-00)"
mkdir -p $LOG_DIR
# Append the process ID to the clamdscan log file name
CLAMSCAN_LOG="${LOG_DIR}/clamdscan_$(date +%H-%M-%S)_$$.log"

# Store the Wazuh alert to the log
echo "$alert" >> $CLAMSCAN_LOG

# Run clamdscan on the file
clamdscan --wait --ping 5 --fdpass $FILE_PATH >> $CLAMSCAN_LOG 2>&1

# Return the exit status of clamdscan
exit $?
