#!/bin/bash

# Notify about updates via Notification Center on Mac OS X or a local system broadcast messages on Linux
#
# Original author: Chris Streeter http://www.chrisstreeter.com
# Requires:
#   - HomeBrew https://docs.brew.sh/Installation
#   - Python 3.* https://www.python.org/
#   - JQ: https://stedolan.github.io/jq/
#   - Node.JS https://nodejs.org/
#   - Node Version Manager https://github.com/nvm-sh/nvm
#   - PIPX https://pypa.github.io/pipx/installation/
#   - mas (for MacOS only) https://github.com/mas-cli/mas
#   - tldr https://tldr.sh/
#   - ble.sh https://github.com/akinomyoga/ble.sh

# env setup
export NVM_LAZY=1
export NVM_DIR=${HOME}/.nvm
export HOMEBREW_NO_ANALYTICS=1
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux
  eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
  [ -s "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh" ] && \. "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # MacOS
  export PATH=/usr/local/opt/python@3.11/bin:/usr/local/opt/python@3.11/libexec/bin:${HOME}/.pyenv/shims:/usr/local/bin:${PATH}
  source /usr/local/opt/nvm/nvm.sh
  eval "$(pyenv init -)"
else
  echo "Unknown OS: $OSTYPE. Platform is not supported."
  exit 1
fi

echo "Script is executed with next user permissions"
id

# HomeBrew
date
echo "Checking HomeBrew updates"
brew update > /dev/null
outdated=$(brew outdated --quiet | sed -e 's/.*\///')
pinned=$(brew list --pinned)
# Remove pinned formulae from the list of outdated formulae
outdated=$(comm -1 -3 <(echo "${pinned}") <(echo "${outdated}"))
if [ -z "${outdated}" ]; then
  echo "No brew updates available"
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    wall -n "No Homebrew updates available yet for any packages."
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    osascript -e 'display notification "No Homebrew updates available yet for any packages." with title "Update notifier" subtitle "No Homebrew updates available"'
  else
    echo "Unknown OS: $OSTYPE. Platform is not supported."
    exit 1
  fi
else
  # We've got an outdated formula or two
  # Notify via Notification Center
  outdated=$(perl -pe 's/\n/, /g' <(echo "${outdated}") | sed 's/, $//g')
  echo "Brew outdated: ${outdated}"
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    wall -n "Homebrew outdated formulae: ${outdated}."
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    osascript -e "display notification \"Outdated formulae: ${outdated}.\" with title \"Update notifier\" subtitle \"Homebrew update(s) available\""
  else
    echo "Unknown OS: $OSTYPE. Platform is not supported."
    exit 1
  fi
fi

# Mac App Store packets
date
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux
  echo "Skip checking for outdated Mac App Store packets on Linux"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # MacOS
  echo "Checking for outdated Mac App Store packets"
  outdated=$(mas outdated | xargs)
  if [ -z "${outdated}" ]; then
    echo "No Mac App Store updates available"
    osascript -e 'display notification "No updates available yet for any Mac App Store packages." with title "Update notifier" subtitle "No Mac App Store updates available"'
  else
    # We've got an outdated global Mac App Store packages
    echo "Outdated Mac App Store packs: ${outdated}"
    osascript -e "display notification \"Outdated Mac App Store packs: ${outdated}.\" with title \"Update notifier\" subtitle \"Mac App Store update(s) available\""
  fi
else
  echo "Unknown OS: $OSTYPE. Platform is not supported."
  exit 1
fi

# pip3
date
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  # Linux
  py_ver=$(python3 --version)
  echo "Skip checking for outdated global ${py_ver} packets on Linux."
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # MacOS
  py_ver=$(python3 --version)
  echo "Checking for outdated global ${py_ver} packets."
  outdated=$(pip3 list --outdated --not-required --format json | jq 'map(.name)' | tr -d '[]\n"')
  if [ -z "${outdated}" ]; then
    echo "No PIP updates available"
    osascript -e "display notification \"No updates available yet for any global ${py_ver} packages.\" with title \"Update notifier\" subtitle \"No ${py_ver} updates available\""
  else
    # We've got an outdated global PIP3 packages
    echo "Outdated ${py_ver} packs: ${outdated}"
    osascript -e "display notification \"Outdated ${py_ver} packs: ${outdated}.\" with title \"Update notifier\" subtitle \"${py_ver} update(s) available\""
  fi
else
  echo "Unknown OS: $OSTYPE. Platform is not supported."
  exit 1
fi

#NPM packets
date
echo "Checking for outdated global NPM packets"
outdated=$(npm outdated --global --json | jq '. |= keys' | tr -d '[]\n"')
if [ -z "${outdated}" ]; then
  echo "No NPM updates available"
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    wall -n "No NPM updates available yet for any global NPM packages."
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    osascript -e 'display notification "No updates available yet for any global NPM packages." with title "Update notifier" subtitle "No NPM updates available"'
  else
    echo "Unknown OS: $OSTYPE. Platform is not supported."
    exit 1
  fi
else
  # We've got an outdated global NPM packages
  echo "Outdated NPM packs: ${outdated}"
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    wall -n "Outdated NPM packs: ${outdated}."
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    osascript -e "display notification \"Outdated NPM packs: ${outdated}.\" with title \"Update notifier\" subtitle \"NPM update(s) available\""
  else
    echo "Unknown OS: $OSTYPE. Platform is not supported."
    exit 1
  fi
fi

# NVM Node.JS
date
echo "Checking for outdated default Node.JS LTS version"
nvm_vers="$(nvm ls-remote --lts --no-colors | grep Latest)"
outdated=$(echo "${nvm_vers}" | grep \>)
last=$(echo "${nvm_vers}" | tail -n 1)
if { { [ -z "${outdated}" ] && [ -n "${nvm_vers}" ]; } ||
  { [ "${outdated}" != "${last}" ] && [ -n "${outdated}" ]; }; }; then
  echo "We have an outdated global Node version: ${outdated}, while newer released: ${last}"
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    wall -n "New Node.JS LTS version available: ${last}."
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    osascript -e "display notification \"${last}.\" with title \"Update notifier\" subtitle \"New Node.JS LTS version available\""
  else
    echo "Unknown OS: $OSTYPE. Platform is not supported."
    exit 1
  fi
else
  echo "No updates available"
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    wall -n "No newer Node.JS LTS versions available."
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    # MacOS
    osascript -e 'display notification "No newer Node.JS LTS versions available." with title "Update notifier" subtitle "No new Node.JS LTS versions available"'
  else
    echo "Unknown OS: $OSTYPE. Platform is not supported."
    exit 1
  fi
fi

date
echo "Upgrading all PIPX packages"
pipx upgrade-all

date
echo "Upgrading tldr cache"
tldr -u

date
echo "Upgrading Ble.sh"
/usr/local/bin/bash $HOME/.local/share/blesh/ble.sh --update

date
echo "--- completed ---"
