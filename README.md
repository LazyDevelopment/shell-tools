# Terms of use #

By using this project or its source code, for any purpose and in any shape or form, you grant your **implicit agreement** to all of the following statements:

- You unequivocally condemn Russia and its military aggression against Ukraine;
- You recognize that Russia is an occupant that unlawfully invaded a sovereign state;
- You agree that [Russia is a terrorist state](https://www.europarl.europa.eu/doceo/document/RC-9-2022-0482_EN.html);
- You fully support Ukraine's territorial integrity, including its claims over [temporarily occupied territories](https://en.wikipedia.org/wiki/Russian-occupied_territories_of_Ukraine);
- You reject false narratives perpetuated by Russian state propaganda.

To learn more about the war and how you can help, [click here](https://war.ukraine.ua/).

Glory to Ukraine! 🇺🇦

- [What is this repository for?](#what-is-this-repository-for)
- [ram-disk](#ram-disk)
    - [Using](#using)
- [test-archive](#test-archive)
    - [Requirements](#requirements)
    - [Setup](#setup)
        - [Set as Quick Action in Finder](#set-as-quick-action-in-finder)
    - [Usage](#usage)
- [ClamAV and VirusTotal scripts](#clamav-and-virustotal)
- [git-repos-upgrade](#git-repos-upgrade)
    - [Usage](#git-repos-upgrade-usage)
- [pip-upgrade / npm-upgrade / node-upgrade](#pip-upgrade--npm-upgrade--node-upgrade)
    - [How do I set them up?](#how-do-i-set-them-up)
    - [pip-upgrade usage](#pip-upgrade-usage)
    - [npm-upgrade usage](#npm-upgrade-usage)
    - [node-upgrade usage](#node-upgrade-usage)
    - [On macOS better works with Update-notifier](#on-macos-better-works-with-update-notifier)
    - [Contribution guidelines](#contribution-guidelines)
    - [Who can I talk to?](#who-can-i-talk-to)

# What is this repository for? <a name="what-is-this-repository-for"></a> #

This is an open-source project to share these simple tools and let others contribute

# ram-disk <a name="ram-disk"></a> #

`ram-disk` is a small and simple, but very useful script to create temporary file storage in your RAM (random access memory) on your Mac. While such storage is strictly limited by the amount of free memory and all the data is lost when it is ejected, it provides the best performance compared to other storage types. So, if you need to perform some heavy file processing operation and you have enough free space in your memory, this would be your best option.

## Using <a name="using"></a> ##

- Get the tool from the [Downloads](<https://bitbucket.org/LazyDevelopment/shell-tools/downloads/>) section of this repo
- Place (or symlink) the SH script somewhere in your PATH and give it execution permission: `chmod +x <path to script>`
- Run the script without any arguments, read the intro and provide the size in MiB of desired RAM Disk storage

# test-archive <a name="test-archive"></a> #

`test-archive` is a shell tool for macOS to test many types of archives for correctness and consistency. It is based on and uses [Keka](<https://www.keka.io/>) to do the testing.

Right now this is a shell tool with the ability to drag-n-drop archives from Finder or choose them interactively. Also, you can add it as a Quick Action to Finder's context menu for files.

This tool has archive format bindings to corresponding Keka modules based on content type (and in some cases on the combination of content type and file extension). There are known archive types and bindings to corresponding modules according to documentation and my testing results. All other archives are assumed to be tested by 7z and UnAr modules. If you have such archives, which are successfully tested by the tool, but in test results, you see that they have "INFO: Processing unexpected file format..." or "INFO: Processing unexpected combination of file format ... and file extension...", please create an [issue](<https://bitbucket.org/LazyDevelopment/shell-tools/issues/new>) with the next info, and I'll add the support for this type:

- Title: `[test-archive] Add support for <file extension>`
- Description: the output of the `test-archive -d <path to your archive>`
- Kind (Type): proposal
- Attachments: smallest possible valid archive of this type

## Requirements <a name="requirements"></a> ##

Please open the script and read it (at least the beginning) to get more details and a clear understanding of how it works.

- Bash 4.2+ `brew info bash` `brew install bash`
- [Keka](<https://www.keka.io/>) `brew install keka`
- terminal-notifier `brew info terminal-notifier` `brew install terminal-notifier`
- [Zenity](<https://wiki.gnome.org/Projects/Zenity>) (for --interactive mode) `brew install zenity`

## Setup <a name="setup"></a> ##

- Supports only macOS, as based on Keka.
- Install all the requirements
- Get the tool from the [Downloads](<https://bitbucket.org/LazyDevelopment/shell-tools/downloads/>) section of this repo
- Place (or symlink) the SH script somewhere in your PATH and give it execution permission: `chmod +x <path to script>`
- Double-check that all the paths at the beginning of the script are correct (everything before the `usage() {` string)

### Set as Quick Action in Finder <a name="set-as-quick-action-in-finder"></a> ###

- Perform all standard Setup steps
- Open the Automator app and create a new Quick Action workflow "Test archive(s)"
- Set the next options in the workflow's settings on top of actions:
  - "Workflow receives current" = "files and folders", "in" = "any application"
  - Other options - as you wish
- In the long list of actions on the left side of the window find the "Run Shell Script" and drag-n-drop it to the free space on the right side under settings area
- In the "Run Shell Script" action set "Shell" to "/bin/bash" and "Pass input" to "as arguments"
- Switch to your favorite terminal application, run Bash shell and execute `echo $PATH`, copy output to the clipboard
- Switch back to Automator's window and in the script area type `export PATH=` and right after that paste that output from your echo in shell
- On a new line of the script put `source <full path to your HOME>/.bashrc`
- On a new line of the script put `<full path to the script folder>/test-archive.sh -d -n ${@} > >(tee -a /usr/local/var/log/test-archive.log) 2> >(tee -a /usr/local/var/log/test-archive.log >&2)`
- Save the workflow and test it by opening the context menu on some archive, find and choose "Quick Actions" > "Test archive(s)"
- As a result, you should get a notification in your Notification Center about each failed test or a summary of all successful tests. Also, in case of failure, you'll see the standard Mac error message with the error details. In any case, the /usr/local/var/log/test-archive.log contains all tool's output for current and previous runs.

## Usage <a name="usage"></a> ##

```bash
Usage (order of options is crucial):

  test-archive.sh [-d | --debug] [-n | --notification] <path to file or folder> ...
  test-archive.sh [-d | --debug] [-n | --notification] -i | --interactive
  test-archive.sh -h | --help
  test-archive.sh -v | --version

Options:
  <path to file or folder>  Relative or absolute path to the archive or folder (directory) with
                            archives to be tested by the tool. You could provide as many paths
                            as you want, separated by the space (each archive is tested separately).
                            Files and folders could be mixed. For each provided folder all files
                            in that folder and all sub-folders will be included into the list for
                            testing. For easy usage you could type in your shell the test-archive.sh and
                            put a space, then multi-select in Finder all the archives, you would like
                            to test, and drag-n-drop them into the shell - it will automatically put
                            their absolute paths as script arguments, separated by space.
  -i --interactive          Allows to interactively select in Finder which of outdated packages you
                            would like to upgrade.
  -n --notification         Send notifications to macOS Notification Center. If all files tested
                            successfully, sends 1 summary notification about that, else - sends failure
                            notification for each failed test with the filename of corresponding archive.
  -d --debug                Show additional DEBUG level messages.
  -h --help                 Show this screen.
  -v --version              Show version.
```

# ClamAV and VirusTotal scripts <a name="clamav-and-virustotal"></a> #

A set of scripts to simplify and automate using the CalmAV solution:

- `clamav-scan` - intended to be called automatically by some file change monitoring solution, so that only new and changed files are scanned. The Wazuh was used as an example file change monitoring solution.
- `clamav-scan-manual`- the script for manual scan of a folder or file. It assumes that the ClamD service is not running all the time, so it starts the service, runs the scan of the file or folder with copying all suspiciuos files to the quarantine folder, and stops the service. Then the script iterates over the quarantine folder and checks all files by VirusTotal saving the result in a text or YML file with similar name at the same folder.
- `vt-check-quarantine`- does only the last part of the `clamav-scan-manual`, but able to check any file or folder.

Usage is pretty simple: `clamav-scan` reads input from STDIN and parses the file path from it; `clamav-scan-manual` and `vt-check-quarantine` expect to get the path as a commandline parameter.

# git-repos-upgrade <a name="git-repos-upgrade"></a> #

`git-repos-upgrade` is a shell tool (wrapper on top of git) to easily upgrade all deliverables (scripts, add-ons, packages, tools), distributed as git repos. By nature, they do not have a way of auto-updates and have to be manually updated by switching to the repo folder and running a sequence of the git commands. With this tool you could define a list of repos in your system, which you'd like to update at once and then run the tool each time, you want to check for updates. It will iterate through your list of repos and check for new commits on the remote for current branch. If updates are found, it will show you the changes and if you'd like to apply them, will try to merge. In case of merge conflict you have to resolve it manually and re-run the tool.

## Usage <a name="git-repos-upgrade-usage"></a> ##

- Make sure that the `readlink` command exists in your system. If not, install it manually
- Create the `git-repos.txt` in the script's folder (the default location) or anywhere in the system (but you'll have to provide the path to it as an optional parameter)
- List paths to the git repos, you'd like to update by the tool: 1 path per line. You could use the `find / 2> /dev/null -name .git -type d -prune` command to find all git repos in your system, but remember to copy paths up to the parent folder for the `.git` folder
- Run the command `git-repos-upgrade.sh [/path/to/the/git-repos.txt]` and follow the instructions

# pip-upgrade / npm-upgrade / node-upgrade <a name="pip-upgrade--npm-upgrade--node-upgrade"></a> #

`pip-upgrade` is a shell tool (wrapper on top of regular PIP) to easily upgrade manually installed global PIP packages in the system, including all their dependencies according to restrictions and constraints, declared in the top-level package dependency declaration.

`npm-upgrade` is a shell tool (wrapper on top of regular NPM) to easily upgrade manually installed global Node.js packages in the system, including all their dependencies according to restrictions and constraints, declared in the top-level package dependency declaration.

`node-upgrade` is a shell tool (wrapper on top of regular NVM) to easily upgrade the system's global Node.js version with the possibility to re-install all global NPM packages from an old version to a new one

## How do I set them up? <a name="how-do-i-set-them-up"></a> ##

- Bash shell is only supported so far, so you need it to use the tool.
  Natively works on Linux / Unix / macOS and other *nix systems.
  Needs additional software (like Cygwin) to work on Windows.
- For `pip-upgrade`, obviously, you need to have Python3 and PIP3 on your system
- For `npm-upgrade`, obviously, you need to have Node.js and NPM on your system
- For `node-upgrade`, you need to have Node Version Manager [https://github.com/nvm-sh/nvm](<https://github.com/nvm-sh/nvm>) with at least 1 Node.js version installed
- JQ tool is required: [https://stedolan.github.io/jq/](<https://stedolan.github.io/jq/>)
- FZF tool is required: [https://github.com/junegunn/fzf](<https://github.com/junegunn/fzf>)
- Get the tool from the [Downloads](<https://bitbucket.org/LazyDevelopment/shell-tools/downloads/>) section of this repo
- Place (or symlink) the SH script somewhere in your PATH and give it execution permission: `chmod +x <path to script>`

## pip-upgrade usage <a name="pip-upgrade-usage"></a> ##

```bash
Usage (order of options is crucial):

  pip-upgrade.sh [-q | --quiet] [-f | --force] -a | --all [-p | --package <package-name>]...
  pip-upgrade.sh [-q | --quiet] [-f | --force] {-p | --package <package-name>}...
  pip-upgrade.sh [-q | --quiet] [-f | --force] -i | --interactive
  pip-upgrade.sh -h | --help
  pip-upgrade.sh -v | --version

Options:
  -i --interactive    Allows to interactively select which of outdated packages you would
                      like to upgrade
  -p --package        Name of the package to be installed or upgraded.
                      This option could be mentioned multiple times: once per package.
                      If specified after --all option, forces install or upgrade of this
                      package (useful for installing new packages or upgrading dependencies
                      of a package, already installed with latest version).
  -a --all            Upgrade all outdated manually installed global packages.
  -f --force          Force upgrade. This will skip user confirmation before upgrade.
  -q --quiet          Do not display log messages.
  -h --help           Show this screen.
  -v --version        Show version.
```

## npm-upgrade usage <a name="npm-upgrade-usage"></a> ##

```bash
Usage (order of options is crucial):

  npm-upgrade.sh [-d | --dry-run] [-f | --force] -a | --all [-p | --package <package-name>]...
  npm-upgrade.sh [-d | --dry-run] [-f | --force] {-p | --package <package-name>}...
  npm-upgrade.sh [-d | --dry-run] [-f | --force] -i | --interactive
  npm-upgrade.sh -h | --help
  npm-upgrade.sh -v | --version

Options:
  -i --interactive    Allows to interactively select which of outdated packages you would
                      like to upgrade
  -p --package        Name of the package to be installed or upgraded.
                      This option could be mentioned multiple times: once per package.
                      If specified after --all option, forces install or upgrade of this
                      package (useful for installing new packages or upgrading dependencies
                      of a package, already installed with latest version).
  -a --all            Upgrade all outdated manually installed global packages.
  -f --force          Force upgrade. This will skip user confirmation before upgrade and
                      download packages from repo even if local copy available.
  -d --dry-run        Test upgrade without real packages modification.
  -h --help           Show this screen.
  -v --version        Show version.

Note: if you have installed NodeJS using NVM, it is recommended to upgrade NPM by the NVM command:
nvm install-latest-npm
```

## node-upgrade usage <a name="node-upgrade-usage"></a> ##

```bash
Usage (order of options is crucial):

  node-upgrade.sh [-d | --keep-default] [-f | --fresh] [-n | --not-update-npm] [-k | --keep-old] -l | --latest
  node-upgrade.sh [-d | --keep-default] [-f | --fresh] [-n | --not-update-npm] [-k | --keep-old] -e | --exact <node-version>
  node-upgrade.sh [-d | --keep-default] [-f | --fresh] [-n | --not-update-npm] [-k | --keep-old] -i | --interactive
  node-upgrade.sh -h | --help
  node-upgrade.sh -v | --version

Options:
  -l --latest           Find out which is the latest Node.JS LTS version released and use it as
                        target version for the upgrade.
  -i --interactive      Allows to interactively select to which of released LTS versions you
                        would like to upgrade. In this case version downgrade scenario is also
                        possible.
  -e --exact            The version of the Node.JS to be installed or upgraded. Version should be
                        provided in exactly same format, like is shown in NVM or returned by the
                        "node --version" command. In this case version downgrade scenario is also
                        possible. Example: v12.22.12
  -f --fresh            Do not re-install NPM packages from current Node.JS version and keep new
                        one fresh and clean.
  -d --keep-default     Do not change default alias. If this flag is not provided, the tool will
                        configure NVM to use latest installed Node.JS version by default.
  -n --not-update-npm   Do not update NPM version to latest one and use the provided one with
                        new Node.JS version.
  -k --keep-old         Do not uninstall current Node.JS version after new one is installed.
  -h --help             Show this screen.
  -v --version          Show version.

WARNING: Make sure to run this tool outside of your Node.JS project. It is designed to upgrade your global
         Node.JS version and will replace your local one for the project if it is different.
```

# On macOS better works with Update-notifier <a name="on-macos-better-works-with-update-notifier"></a> #

I have the Update-notifier script, based on [https://gist.github.com/streeter/3254906](<https://gist.github.com/streeter/3254906>)
It is scheduled to be executed after login and at 10 AM each day, and checks for updates in Homebrew, Mac App Store, PIP, NPM global packages, Node.js default LTS version.
The example is also in this repo in the Update-notifier folder.
This is a template, which works for me, but most probably you'd like to modify it according to your needs. Anyway, please read it 1st.

# Contribution guidelines <a name="contribution-guidelines"></a> #

If you want to contribute, create a PR, and I'll review it

# Who can I talk to? <a name="who-can-i-talk-to"></a> #

Repo owner: Kostiantyn aka LazyDevelopment

Please feel free to create an [issue](<https://bitbucket.org/LazyDevelopment/shell-tools/issues/new>) with any thoughts about these tools, enhancements, or bug reports... I'll review them and get back to you.
