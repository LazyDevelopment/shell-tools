#!/bin/bash
# This script iteratively goes through the git repos from the `git-repos.txt`
# and for each repo tries to pull the latest commit from remote for current branch.

# If there will be a merge conflict detected, the script will show it to you
# and ask to resolve it manually, then it will jump to the next repo in the list.

# For reach successfully updated repo it will run garbage collection at the end.

# REQUIREMENTS:
# - `git-repos.txt` by default should be located at the same folder with this script,
#   but you could provide the path to it in any resolvable form as an execution parameter:
#   `git-repos-upgrade.sh /path/to/git-repos.txt`
# - `find / 2> /dev/null -name .git -type d -prune` command could be used to find all
#   git repos in your system. Choose needed ones from the results and add to the
#   `git-repos.txt` (1 per line). Make sure to add the parent folder of the `.git` folder.
# - `readlink` might not exist in your system. If that is the case, install it manually.

# File with the list of repositories to update
repo_file=${1:-"$(dirname "$(readlink -f "$0")")/git-repos.txt"}

# Check if file exists
if [[ ! -f $repo_file ]]; then
  echo "File $repo_file does not exist. If it is located elsewhere, append the path to it as a parameter."
  exit 1
fi

# Read repositories from file
repos=()
while IFS= read -r line; do
  repos+=("$line")
done < "$repo_file"

for repo in "${repos[@]}"; do
  echo "------------------------------------------------------------------"
  echo "Updating $repo"
  # shellcheck disable=SC2106
  cd "$repo" || ( echo "Unable to switch to the $repo folder."; continue )

  # Fetch updates
  git fetch

  # Check if the repository is up-to-date
  UPSTREAM='@{u}'
  LOCAL=$(git rev-parse @)
  REMOTE=$(git rev-parse "$UPSTREAM")
  if [ $LOCAL = $REMOTE ]; then
      echo "Up-to-date"
      continue   # If the repo is up-to-date, it will skip to the next repo.
  fi

  # Show updates
  read -n1 -s -r -p "Updates found. Press any key to continue..."; echo
  git log HEAD..origin --oneline

  # Ask for user confirmation
  read -p "Do you want to update this repo (y/n)? " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    # Attempt to merge
    #git merge origin --no-commit
    git pull

    # Check for conflicts
    # if git ls-files -u | grep -q ""; then
    if [[ $$? -ne 0 ]]; then
      echo "Merge conflict in $repo. Resolve this and then run the script again."
      git merge --abort
    else
      git commit -m "Merged updates"
      git gc
    fi
  fi

done
