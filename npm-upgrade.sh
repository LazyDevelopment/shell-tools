#!/bin/bash

# Upgrade all globally installed NPM packages with their dependencies to latest versions
# according to dependency declaration trees and constraints
# Requires:
# - JQ: https://stedolan.github.io/jq/
# - FZF: https://github.com/junegunn/fzf

SCRIPT=$(basename "${0}")
readonly SCRIPT
readonly VERSION='1.0.1'

usage() {
  cat <<EOF
Usage (order of options is crucial):

  ${SCRIPT} [-d | --dry-run] [-f | --force] -a | --all [-p | --package <package-name>]...
  ${SCRIPT} [-d | --dry-run] [-f | --force] {-p | --package <package-name>}...
  ${SCRIPT} [-d | --dry-run] [-f | --force] -i | --interactive
  ${SCRIPT} -h | --help
  ${SCRIPT} -v | --version

Options:
  -i --interactive    Allows to interactively select which of outdated packages you would
                      like to upgrade
  -p --package        Name of the package to be installed or upgraded.
                      This option could be mentioned multiple times: once per package.
                      If specified after --all option, forces install or upgrade of this
                      package (useful for installing new packages or upgrading dependencies
                      of a package, already installed with latest version).
  -a --all            Upgrade all outdated manually installed global packages.
  -f --force          Force upgrade. This will skip user confirmation before upgrade and
                      download packages from repo even if local copy available.
  -d --dry-run        Test upgrade without real packages modification.
  -h --help           Show this screen.
  -v --version        Show version.

Note: if you have installed NodeJS using NVM, it is recommended to upgrade NPM by the NVM command:
nvm install-latest-npm
EOF
}

print_message() {
  printf "%s\n" "${1}"
}

# Checking list of shell arguments
while [[ ${#} -gt 0 ]]; do
  key="${1}"

  case ${key} in
  -i | --interactive)
    print_message "INFO: Collecting list of outdated manually installed global NPM packages"
    outdated=$(npm outdated --global --json | jq)
    print_message "${outdated}"
    outdated=$(echo "${outdated}" | jq '. |= keys' | tr -d '[],"' | awk NF)
    if [ -n "${outdated}" ]; then
      echo
      print_message "Note: if you've installed NodeJS using NVM, it is recommended to upgrade NPM by the NVM command:"
      print_message "nvm install-latest-npm"
      echo
      # Choose which packets will be upgraded
      print_message "INFO: Please choose which packages to upgrade."
      print_message "INFO: Up, Down to navigate; type to filter; TAB to (un)select; Enter to confirm."
      outdated=$(echo "${outdated}" | fzf -m --cycle --height=~1% | xargs)
    else
      print_message "INFO: There are no outdated manually installed global packages. Skipping of dependencies update. Nothing to do."
      exit 0
    fi
    i=1
    ;;
  -a | --all)
    print_message "INFO: Collecting list of outdated manually installed global NPM packages"
    outdated=$(npm outdated --global --json | jq)
    print_message "${outdated}"
    echo
    print_message "Note: if you've installed NodeJS using NVM, it is recommended to upgrade NPM by the NVM command:"
    print_message "nvm install-latest-npm"
    echo
    # Prepare outdated list for internal use
    outdated=$(echo "${outdated}" | jq '. |= keys' | tr -d '[],"\n' | xargs)
    a=1
    ;;
  -p | --package)
    outdated="${outdated} ${2}"
    p=1
    shift
    ;;
  -f | --force)
    FORCE=true
    frce="--force"
    ;;
  -d | --dry-run)
    dr="--dry-run"
    ;;
  -h | --help)
    usage
    exit 0
    ;;
  -v | --version)
    printf "%s\n" ${VERSION}
    exit 0
    ;;
  *)
    (printf >&2 "ERROR: Unknown parameter: %s\n" "${1}")
    usage
    exit 1
    ;;
  esac
  shift
done

# Check that at least 1 required option is set
if { [ -z "${a}" ] && [ -z "${p}" ] && [ -z "${i}" ]; } || [ -z "${key}" ]; then
  echo ERROR: Any of required options were not provided. Unsupported invocation.
  usage
  exit 1
fi

if [ -z "${outdated}" ]; then
  echo
  print_message "INFO: No manually installed global packages are selected for upgrade. Skipping of dependencies update. Nothing to do."
else
  # We've got an outdated global NPM packages
  outdated=$(echo "${outdated}" | xargs) # trim string
  if [ ! "${FORCE}" ]; then
    read -p "Are you sure to install/upgrade <${outdated}> global package(s) with dependencies? [y/n] " -n 1 -r
    echo # move to a new line
    if [[ ${REPLY} =~ ^[Yy]$ ]]; then
      # Upgrade each found outdated or requested package and try to update all its dependencies if possible according to dependency constraints
      # shellcheck disable=SC2086
      npm install --global ${outdated} ${frce} ${dr}
    fi
  else
    # Upgrade each found outdated or requested package and try to update all its dependencies if possible according to dependency constraints
    # shellcheck disable=SC2086
    npm install --global ${outdated} ${frce} ${dr}
  fi
fi
