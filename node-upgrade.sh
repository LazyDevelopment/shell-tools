#!/bin/bash

# Upgrade globally installed Node.JS version to the latest LTS version released
# Requires:
# - Node Version Manager https://github.com/nvm-sh/nvm
# - FZF: https://github.com/junegunn/fzf

SCRIPT=$(basename "${0}")
readonly SCRIPT
readonly TOOL_VERSION='1.0.0'

usage() {
  cat <<EOF
Usage (order of options is crucial):

  ${SCRIPT} [-d | --keep-default] [-f | --fresh] [-n | --not-update-npm] [-k | --keep-old] -l | --latest
  ${SCRIPT} [-d | --keep-default] [-f | --fresh] [-n | --not-update-npm] [-k | --keep-old] -e | --exact <node-version>
  ${SCRIPT} [-d | --keep-default] [-f | --fresh] [-n | --not-update-npm] [-k | --keep-old] -i | --interactive
  ${SCRIPT} -h | --help
  ${SCRIPT} -v | --version

Options:
  -l --latest           Find out which is the latest Node.JS LTS version released and use it as
                        target version for the upgrade.
  -i --interactive      Allows to interactively select to which of released LTS versions you
                        would like to upgrade. In this case version downgrade scenario is also
                        possible.
  -e --exact            The version of the Node.JS to be installed or upgraded. Version should be
                        provided in exactly same format, like is shown in NVM or returned by the
                        "node --version" command. In this case version downgrade scenario is also
                        possible. Example: v12.22.12
  -f --fresh            Do not re-install NPM packages from current Node.JS version and keep new
                        one fresh and clean.
  -d --keep-default     Do not change default alias. If this flag is not provided, the tool will
                        configure NVM to use latest installed Node.JS version by default.
  -n --not-update-npm   Do not update NPM version to latest one and use the provided one with
                        new Node.JS version.
  -k --keep-old         Do not uninstall current Node.JS version after new one is installed.
  -h --help             Show this screen.
  -v --version          Show version.

WARNING: Make sure to run this tool outside of your Node.JS project. It is designed to upgrade your global
         Node.JS version and will replace your local one for the project if it is different.
EOF
}

get_version_from_str() {
  IFS=' '
  read -ra strarr <<< "${1}"
  for val in "${strarr[@]}";
  do
    if [[ ${val} =~ ^v([0-9]+).([0-9]+).([0-9]+) ]]; then
      echo "${val}"
    fi
  done
}

# env setup
export NVM_LAZY=1
export NVM_DIR=${HOME}/.nvm;
# shellcheck disable=SC1091
[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh"  # This loads nvm

# Checking list of shell arguments
while [[ ${#} -gt 0 ]]; do
  key="${1}"

  case ${key} in
  -i | --interactive)
    i=1
    ;;
  -l | --latest)
    lst=1
    ;;
  -e | --exact)
    last_node="${2}"
    e=1
    shift
    ;;
  -d | --keep-default)
    d=true
    ;;
  -f | --fresh)
    f=true
    ;;
  -n | --not-update-npm)
    n=true
    ;;
  -k | --keep-old)
    k=true
    ;;
  -h | --help)
    usage
    exit 0
    ;;
  -v | --version)
    printf "%s\n" ${TOOL_VERSION}
    exit 0
    ;;
  *)
    (printf >&2 "ERROR: Unknown parameter: %s\n" "${1}")
    usage
    exit 1
    ;;
  esac
  shift
done

# Check that at least 1 required option is set
if { [ -z "${lst}" ] && [ -z "${e}" ] && [ -z "${i}" ]; } || [ -z "${key}" ]; then
  echo "ERROR: Any of required options were not provided. Unsupported invocation."
  usage
  exit 1
fi

# Show the current state
echo "WARNING: Make sure to run this tool outside of your Node.JS project. It is designed to upgrade your global"
echo "         Node.JS version and will replace your local one for the project if it is different"
echo
echo "INFO: Here is your current NVM local Node.JS setup:"
nvm ls
echo
nvm_ls_remote="$(nvm ls-remote --lts --no-colors | grep Latest)"
echo "INFO: Here is the list of released Node.JS LTS latest versions:"
echo "${nvm_ls_remote}"
echo
current_node="$(node --version)"
if [ -z "${current_node}" ]; then
    echo "WARNING: There is no Node.JS installed or the installed version is not accessible. Check whether it is used."
    echo "INFO: Nothing to do so far."
    exit 1
  else
    echo "INFO: Current globally accessible version of Node.JS is ${current_node}"
fi
echo "INFO: Here is the list of globally installed NPM packages:"
npm list --global --depth 0
echo

# Case "latest"
if [ -n "${lst}" ]; then
  last_node=$( get_version_from_str "$(echo "${nvm_ls_remote}" | tail -n 1 | xargs)" )
  if [ -z "${last_node}" ]; then
      echo "CRITICAL: Something went wrong. Not possible to detect latest version of Node.JS!"
      echo "INFO: Exiting"
      exit 1
    else
      echo "INFO: Latest released LTS version of Node.JS is ${last_node}"
  fi
  if [[ "${current_node}" == "${last_node}" ]]; then
    echo "WARNING: Current globally accessible version of Node.JS in your system is the same as latest released one."
    echo "INFO: Nothing to do so far."
    exit 0
  fi
fi

# Case "interactive"
if [ -n "${i}" ]; then
  echo "INFO: Please choose which Node.JS version would be the replacement for currently installed one."
  echo "INFO: Up, Down to navigate; type to filter; TAB to (un)select; Enter to confirm."
  last_node=$( get_version_from_str "$(nvm ls-remote --no-colors | fzf --cycle --height=70% | xargs)" )
  if [ -z "${last_node}" ]; then
    echo "CRITICAL: Something went wrong. Not possible to detect chosen version of Node.JS!"
    echo "INFO: Exiting"
    exit 1
  fi
  if [[ "${current_node}" == "${last_node}" ]]; then
    echo "WARNING: Current globally accessible version of Node.JS in your system is the same as chosen released one."
    echo "INFO: Nothing to do so far."
    exit 0
  fi
fi

# Case "exact"
if [ -n "${e}" ]; then
  echo "INFO: You've provided next Node.JS version to be the replacement for currently installed one: ${last_node}"
  if [ -z "${last_node}" ]; then
    echo "CRITICAL: There is no Node.JS version provided!"
    usage
    echo "INFO: Exiting"
    exit 1
  fi
  if ! [[ ${last_node} =~ ^v([0-9]+).([0-9]+).([0-9]+) ]]; then
    echo "ERROR: The ${last_node} is not formatted correctly. Correct format example is v12.22.12"
    echo "INFO: Exiting"
    exit 1
  fi
  if [[ "${current_node}" == "${last_node}" ]]; then
    echo "WARNING: Current globally accessible version of Node.JS in your system is the same as provided released one."
    echo "INFO: Nothing to do so far."
    exit 0
  fi
fi

echo
echo "INFO:  Starting the Node.JS version upgrade from ${current_node} to ${last_node}"
if [ "${f}" ]; then
    nvm install "${last_node}"
    echo "INFO: Node.JS ${last_node} is installed successfully."
  else
    nvm install "${last_node}" --reinstall-packages-from="${current_node}"
    echo "INFO: Node.JS ${last_node} is installed successfully."
    echo "INFO: All global NPM packages from the ${current_node} version were re-installed into ${last_node} as well."
fi
if [ ! "${d}" ]; then
  echo "INFO: Set default Node.JS version to be latest installed one."
  nvm alias default node
fi
echo "INFO: Activating just installed Node.JS ${last_node}"
nvm use "${last_node}"
echo "INFO: Node.JS ${last_node} is activated."
if [ ! "${n}" ]; then
  echo "INFO: Installing latest NPM"
  nvm install-latest-npm
  echo "INFO: Latest version of NPM is installed for Node.JS ${last_node}."
fi
if [ ! "${k}" ]; then
  echo "INFO: Uninstalling Node.JS ${current_node} version."
  nvm uninstall "${current_node}"
  echo "INFO: The Node.JS ${current_node} was uninstalled successfully"
fi

new_node="$(node --version)"
if [[ "${new_node}" != "${last_node}" ]] || [[ "${new_node}" == "${current_node}" ]]; then
  echo "CRITICAL: Something went wrong. Node.JS version is still not the ${last_node} even after upgrade!"
  echo "INFO: Exiting"
  exit 1
fi
echo
echo "INFO: Here is how NVM local Node.JS setup looks like:"
nvm ls
echo
echo "INFO: This is currently active Node.JS binary"
which node
echo
echo "INFO: Here is the list of globally installed NPM packages:"
npm list --global --depth 0
echo
echo "INFO: Let's run 'npm doctor' to make sure everything works fine"
npm doctor
echo
echo "WARNING: Because of the specific of NVM and Bash scripting, you need to reopen your shell or"
echo "         re-initialize it to apply the changes. Please reopen or re-initialize your shell"
