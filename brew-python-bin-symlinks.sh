#!/usr/bin/env bash

## This script recreates Homebrew's symlinks for the Python to point all the Python 3 related
## symlinks in the bin folder to the Python 3.11 version.
## It is useful if you have Python 3.11 and 3.12 installed, and want to have the 3.11 to be the system
## default version, while `brew upgrade` always recreate them to point to the 3.12 version.

# Check the architecture of the Mac
if [[ $(uname -m) == "arm64" ]]; then
    # If it's ARM-based, set the directory to /opt/homebrew/bin
    dir="/opt/homebrew/bin"
else
    # If it's Intel-based, set the directory to /usr/local/bin
    dir="/usr/local/bin"
fi

# Define the symlinks to create
declare -A links=(
    ["2to3"]="2to3-3.11"
    ["idle3"]="idle3.11"
    ["pip3"]="pip3.11"
    ["pydoc3"]="pydoc3.11"
    ["python3"]="python3.11"
    ["python3-config"]="python3.11-config"
    ["wheel3"]="wheel3.11"
)

# Loop over the symlinks to create
for link in "${!links[@]}"; do
    # Check if the target file exists
    if [ ! -e "$dir/${links[$link]}" ]; then
        echo "Error: Target file ${links[$link]} does not exist. Skipping $link."
        continue
    fi

    # Check if the symlink exists
    if [ -L "$dir/$link" ]; then
        echo "Found '$dir/$link -> $(readlink "$dir/$link")'"

        # If the symlink points to a different target, update it
        if [ "$(readlink "$dir/$link")" != "./${links[$link]}" ]; then
            ln -sf "./${links[$link]}" "$dir/$link"
            echo "Updated '$dir/$link -> $(readlink "$dir/$link")'"
        else
            # If the symlink points to the correct target, do nothing
            echo "'$dir/$link' is correct, nothing to do."
        fi
    else
        # If the symlink doesn't exist, create it
        ln -s "./${links[$link]}" "$dir/$link"
        echo "Created '$dir/$link -> $(readlink "$dir/$link")'"
    fi
done

echo "Symlinks tested/created/updated successfully."
