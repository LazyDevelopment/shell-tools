#!/usr/local/bin/bash
# Make sure ^ is your latest version of Bash executable located

# Requires:
# - Bash 4.2+ (brew install bash)
# - Keka: https://www.keka.io/ (brew install keka)
# - Zenity (for --interactive mode): https://wiki.gnome.org/Projects/Zenity (brew install zenity)

# Exit statuses:
# - 0: Everything OK, all archives are fine.
# - 1: Internal error. Maybe bug or something is missing.
# - 2: At least 1 archive is corrupted
# - 3: At least 1 mandatory argument required

# Uncomment next line to enable trace mode output:
#set -x

SCRIPT=$(basename "${0}")
ORIGINAL_PWD="${PWD}"
KEKA_PATH="/Applications/Keka.app/Contents/MacOS"
options=( "${@}" )
declare -A res_status_map
readonly SCRIPT
readonly ORIGINAL_PWD
readonly KEKA_PATH
readonly VERSION='1.0.0'

usage() {
  cat <<EOF
Usage (order of options is crucial):

  ${SCRIPT} [-d | --debug] [-n | --notification] <path to file or folder> ...
  ${SCRIPT} [-d | --debug] [-n | --notification] -i | --interactive
  ${SCRIPT} -h | --help
  ${SCRIPT} -v | --version

Options:
  <path to file or folder>  Relative or absolute path to the archive or folder (directory) with
                            archives to be tested by the tool. You could provide as many paths
                            as you want, separated by the space (each archive is tested separately).
                            Files and folders could be mixed. For each provided folder all files
                            in that folder and all sub-folders will be included into the list for
                            testing. For easy usage you could type in your shell the ${SCRIPT} and
                            put a space, then multi-select in Finder all the archives, you would like
                            to test, and drag-n-drop them into the shell - it will automatically put
                            their absolute paths as script arguments, separated by space.
  -i --interactive          Allows to interactively select in Finder which of outdated packages you
                            would like to upgrade.
  -n --notification         Send notifications to macOS Notification Center. If all files tested
                            successfully, sends 1 summary notification about that, else - sends failure
                            notification for each failed test with the filename of corresponding archive.
  -d --debug                Show additional DEBUG level messages.
  -h --help                 Show this screen.
  -v --version              Show version.
EOF
}

print_message() {
  # ${1} : Level (DEBUG, INFO, WARNING, ERROR)
  # ${2} : Message
  if [[ "${1}" == "DEBUG" ]] && [ "${debug}" ]; then
      printf "%s: %s\n" "${1}" "${2}"
    elif [[ "${1}" == "ERROR" ]]; then
      >&2 printf "%s: %s\n" "${1}" "${2}"
    elif [[ "${1}" != "DEBUG" ]]; then
      printf "%s: %s\n" "${1}" "${2}"
  fi
}

get_abs_filename() {
  # ${1} : relative filename
  filename=${1}
  parentdir=$(dirname "${filename}")

  if [ -d "${filename}" ]; then
    # shellcheck disable=SC2005
    echo "$(cd "${filename}" && pwd)"
  elif [ -d "${parentdir}" ]; then
    echo "$(cd "${parentdir}" && pwd)/$(basename "${filename}")"
  fi
  cd "${ORIGINAL_PWD}" || true
}

testing_archive() {
  case ${file_type} in
  x-7z-compressed | zip | x-ms-wim | zlib | x-iso9660-image | x-msi | java-archive | vnd.ms-cab-compressed | x-dosexec | x-xar)
    print_message "INFO" "Testing it using 7z library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli 7zz t "${file_path}" \
      || res_status_map["${file_path}"]="7z"
    ;;
  x-rar)
    print_message "INFO" "Testing it using RAR library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli unrar t "${file_path}" \
      || res_status_map["${file_path}"]="RAR"
    ;;
  x-tar)
    print_message "INFO" "Testing it using TAR library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli tar -x -f "${file_path}" -O > /dev/null \
      || res_status_map["${file_path}"]="TAR"
    ;;
  gzip)
    print_message "INFO" "Testing it using Gzip library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli pigz --test "${file_path}" \
      || res_status_map["${file_path}"]="Gzip"
    ;;
  x-bzip2)
    print_message "INFO" "Testing it using Bzip2 library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli pbzip2 --test "${file_path}" \
      || res_status_map["${file_path}"]="Bzip2"
    ;;
  x-xz)
    print_message "INFO" "Testing it using XZ library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli xz --test "${file_path}" \
      || res_status_map["${file_path}"]="XZ"
    ;;
  x-lzip)
    print_message "INFO" "Testing it using Lzip library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli plzip --test "${file_path}" \
      || res_status_map["${file_path}"]="Lzip"
    ;;
  zstd)
    print_message "INFO" "Testing it using ZSTD library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli zstd --test "${file_path}" \
      || res_status_map["${file_path}"]="ZSTD"
    ;;
  octet-stream)
    IFS='.'
    read -ra strarr <<< "${file_path}"
    ext="${strarr[-1]}"
    if [[ "${ext}" == "DS_Store" ]]; then
        print_message "DEBUG" "Standard macOS file .DS_Store detected. Skipping it as it is not an archive."
      elif [[ "${ext}" == "lrz" ]]; then
        print_message "INFO" "Testing it using LRzip library"
        "${KEKA_PATH}"/Keka --ignore-file-access --cli lrzip --test "${file_path}" \
          || res_status_map["${file_path}"]="LRzip"
      elif [[ "${ext}" == "br" ]]; then
        print_message "INFO" "Testing it using Brotli library"
        "${KEKA_PATH}"/Keka --ignore-file-access --cli brotli --test "${file_path}" \
          || res_status_map["${file_path}"]="Brotli"
      elif [[ "${ext}" == "aar" ]]; then
        print_message "INFO" "Testing it using macOS built-in AA tool"
        mkdir -p "/tmp/test-archive"
        aa extract -v -i "${file_path}" -d "/tmp/test-archive" \
          || res_status_map["${file_path}"]="AA"
        rm -rf "/tmp/test-archive"
      else
        print_message "INFO" "Processing unexpected combination of file format ${file_type} and file extension ${ext}."
        print_message "INFO" "Trying to parse the file as an archive: ${file_path}"
        print_message "INFO" "Using 7z library"
        "${KEKA_PATH}"/Keka --ignore-file-access --cli 7zz t "${file_path}" \
          || res_status_map["${file_path}:7z"]="Unknown"
        print_message "INFO" "Using UnAr library"
        "${KEKA_PATH}"/Keka --ignore-file-access --cli unar -o - "${file_path}" > /dev/null \
          || res_status_map["${file_path}:UnAr"]="Unknown"
    fi
    ;;
  *)
    print_message "INFO" "Processing unexpected file format ${file_type} and trying to parse the file as an archive: ${file_path}"
    print_message "INFO" "Using 7z library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli 7zz t "${file_path}" \
      || res_status_map["${file_path}:7z"]="Unknown"
    print_message "INFO" "Using UnAr library"
    "${KEKA_PATH}"/Keka --ignore-file-access --cli unar -o - "${file_path}" > /dev/null \
      || res_status_map["${file_path}:UnAr"]="Unknown"
    ;;
  esac
#   2 more commands for future just in case:
#   "${KEKA_PATH}"/Keka --ignore-file-access --cli lzip --test "${file_path}"
#   "${KEKA_PATH}"/Keka --ignore-file-access --cli lbzip2 --test "${file_path}"
}

print_message "DEBUG" "Script got ${#options[@]} arguments: ${options[*]}"
# check that at least 1 argument is provided
if [[ "${#options[@]}" == 0 ]]; then
    print_message "ERROR" "At least 1 mandatory argument required."
    echo
    usage
    exit 3
fi

# Checking list of shell arguments
i=0
count=0
while [[ ${i} -lt ${#options[@]} ]]; do
  key="${options[i]}"

  case ${key} in
  -h | --help)
    usage
    exit 0
    ;;
  -v | --version)
    printf "%s\n" ${VERSION}
    exit 0
    ;;
  -d | --debug)
    debug=1
    ;;
  -n | --notification)
    notification=1
    ;;
  -i | --interactive)
    print_message "DEBUG" "Round ${i}"
    print_message "WARNING" "The file selection dialogue may start on the background because of lib's known bug."
    print_message "WARNING" "If that is the case, please switch to it using any convenient way (like CMD + Tab)."
    IFS=' '
    # shellcheck disable=SC2207
    options+=($(zenity --file-selection --multiple --filename "${ORIGINAL_PWD}/" --separator " "))
    print_message "INFO" "$(( ${#options[@]} - i - 1 )) file(s) going to be tested"
    ;;
  *)
    echo "==========================================================================="
    (( count++ )) || true
    print_message "INFO" "${count}. Processing the file : ${key}"
    file_path="$( get_abs_filename "${key}" )"
    if [ -f "${file_path}" ]; then
        print_message "DEBUG" "File path is ${file_path}"
        file_type="$(file -b --mime-type "${file_path}")"
        file_subtype="$(file -b -z --mime-type "${file_path}" || true )"
        IFS='/'
        read -ra strarr <<< "${file_type}"
        file_type="${strarr[1]}"
        read -ra strarr <<< "${file_subtype}"
        file_subtype="${strarr[1]}"
        print_message "INFO" "File type is ${file_type}"
        print_message "DEBUG" "File sub-type is ${file_subtype}"
        testing_archive
      elif [ -d "${file_path}" ]; then
        while IFS='' read -r line
          do options+=("${line}")
        done < <(find "${file_path}" -type f)
        print_message "INFO" "Folder detected, all files in it and sub-folders added for processing"
        print_message "DEBUG" "Now there are ${#options[@]} arguments"
        print_message "DEBUG" "And args are: ${options[*]}"
      else
        print_message "ERROR" "File or folder ${file_path} doesn't exist or not enough permissions to use it."
    fi
    ;;
  esac
  (( i++ )) || true
done
echo "==========================================================================="

if [ ${#res_status_map[@]} -gt 0 ]; then
    IFS=$'\n'
    keys=("$(sort <<<"${!res_status_map[@]}")")
    echo
    print_message "ERROR" "Next archive(s) corrupted (other ones are ok):"
    # shellcheck disable=SC2048
    for key in ${keys[*]}; do
      output+="${res_status_map[${key}]}: ${key}
"
      if [ "${notification}" ]; then
        osascript -e "display notification \"Test of $(basename ${key}) is failed.\" with title \"Test archive\" subtitle \"Failed test of $(basename ${key})\""
      fi
    done
    >&2 echo "${output}"
    exit 2
  else
    print_message "INFO" "All archives were tested successfully."
    if [ "${notification}" ]; then
      osascript -e 'display notification "All archives were tested successfully." with title "Test archive" subtitle "All archives are OK"'
    fi
fi
